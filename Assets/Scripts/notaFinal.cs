﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class notaFinal : MonoBehaviour
{
   private int idTema;
   public Text txtNota;
   public Text txtInfoTema;

   public GameObject estrela1;
   public GameObject estrela2;
   public GameObject estrela3;
   private int notaFfinal;
   private int acertos;

    void Start()
    {
        idTema= PlayerPrefs.GetInt("idTema");
        estrela1.SetActive(false);
        estrela2.SetActive(false);
        estrela3.SetActive(false);

      
        notaFfinal = PlayerPrefs.GetInt("notaFinalTemp"+ idTema.ToString());
        acertos = PlayerPrefs.GetInt("acertosTemp"+idTema.ToString());

        txtNota.text = notaFfinal.ToString();
        txtInfoTema.text = "Você acertou " + acertos.ToString()+ " de 3 perguntas";

        switch(notaFfinal){           

            case 3:
                estrela1.SetActive(true);
                estrela2.SetActive(false);
                estrela3.SetActive(false);

            break;

            case 7:
                estrela1.SetActive(true);
                estrela2.SetActive(true);
                estrela3.SetActive(false);

            break;
            case 10:
                estrela1.SetActive(true);
                estrela2.SetActive(true);
                estrela3.SetActive(true);

            break;
            case 0:
            estrela1.SetActive(false);
            estrela2.SetActive(false);
            estrela3.SetActive(false);
            break;
        }
    }

    // Update is called once per frame
    public void jogarNovamente()
    {
        Application.LoadLevel("Scene_T"+idTema.ToString());
    }
}
