﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class control_Tema : MonoBehaviour
{
    public Button btnPlay;
    public Text txtNomeTema;
    public Text txtInfoTema;
    public GameObject infoTema;
    public GameObject estrela1;
    public GameObject estrela2;
    public GameObject estrela3;
    public string[] nomeTema;
    private int idTema;
    public int numeroQuestoes;

    // Start is called before the first frame update
    void Start()
    {
        idTema = 0;        
        txtNomeTema.text = nomeTema[idTema];
        txtInfoTema.text = "Você acertou 0 de 20 quesotões";
        infoTema.SetActive(false);
        estrela1.SetActive(false);
        estrela2.SetActive(false);
        estrela3.SetActive(false);
        btnPlay.interactable = false;
    }

    public void SelecionaTema(int i){
        idTema = i;
        PlayerPrefs.SetInt("idTema", idTema);
        txtNomeTema.text = nomeTema[idTema];
        int notaFinal = PlayerPrefs.GetInt("notaFinal"+ idTema.ToString());
        int acertos = PlayerPrefs.GetInt("acertos"+ idTema.ToString());

        estrela1.SetActive(false);
        estrela2.SetActive(false);
        estrela3.SetActive(false);

         switch(notaFinal){           

            case 3:
                estrela1.SetActive(true);
                estrela2.SetActive(false);
                estrela3.SetActive(false);

            break;

            case 7:
                estrela1.SetActive(true);
                estrela2.SetActive(true);
                estrela3.SetActive(false);

            break;
            case 10:
                estrela1.SetActive(true);
                estrela2.SetActive(true);
                estrela3.SetActive(true);

            break;
            case 0:
            estrela1.SetActive(false);
            estrela2.SetActive(false);
            estrela3.SetActive(false);
            break;
            
        }

        txtInfoTema.text = "Você acertou "+ acertos.ToString() +" de " + numeroQuestoes.ToString() + " questões!";
        infoTema.SetActive(true);
        btnPlay.interactable = true;

    }
    public void Jogar(){

        Application.LoadLevel("Scene_T" + idTema.ToString());
    }
    
}
