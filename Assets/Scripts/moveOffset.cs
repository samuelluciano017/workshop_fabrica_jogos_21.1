﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveOffset : MonoBehaviour
{
    private Material materialAtual;
    public float velocidade;
    public float offSet;
    


    // Start is called before the first frame update
    void Start()
    {
        materialAtual = GetComponent<Renderer>().material;
        velocidade = 1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        offSet += 0.001f;

        materialAtual.SetTextureOffset("_MainTex", new Vector2(offSet * velocidade, 0));
    }
}
