﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Responder : MonoBehaviour
{
   private int idTema;
   public Text pergunta;
   public Text respostaA;
   public Text respostaB;
   public Text respostaC;
   public Text respostaD;
   public Text infoRespotas;
   public string[] perguntas; // armazena perguntas
   public string[] alternativasA; // armazena as alternaivas A
   public string[] alternativasB; // armazena as alternaivas B
   public string[] alternativasC; // armazena as alternaivas C
   public string[] alternativasD; // armazena as alternaivas D
   public string[] corretas;  // armazena as alternaivas corretas
    private int idPergunta;
    private float acertos = 0;
    private float questoes;
    private float media;
    private int notaFinal;

    void Start()
    {
        idTema = PlayerPrefs.GetInt("idTema");
        idPergunta = 0;
        questoes = perguntas.Length;
        pergunta.text = perguntas[idPergunta];
        respostaA.text = alternativasA[idPergunta]; 
        respostaB.text = alternativasB[idPergunta];     
        respostaC.text = alternativasC[idPergunta];
        respostaD.text = alternativasD[idPergunta];      

        infoRespotas.text = "Respondendo "+ (idPergunta+1).ToString()+ " de " + questoes.ToString()+" perguntas";    
                 
    }
    public void respostas(string alternativa){

        switch(alternativa){

            case "A":
            if (alternativasA[idPergunta] == corretas[idPergunta]){

                acertos++;
            }
            break;

            case "B":
             if (alternativasB[idPergunta] == corretas[idPergunta]){

                acertos++;
            }
            break;

            case "C":
             if (alternativasC[idPergunta] == corretas[idPergunta]){

                acertos++;
            }
            break;

            case "D":
             if (alternativasD[idPergunta] == corretas[idPergunta]){

                acertos++;
            }
            break;

        }
        proximaPergunta();
    }
    void proximaPergunta(){

        idPergunta++;

        if (idPergunta<= (questoes-1)){
            pergunta.text = perguntas[idPergunta];
            respostaA.text = alternativasA[idPergunta]; 
            respostaB.text = alternativasB[idPergunta];     
            respostaC.text = alternativasC[idPergunta];
            respostaD.text = alternativasD[idPergunta];      

        infoRespotas.text = "Respondendo "+ (idPergunta+1).ToString()+ " de " + questoes.ToString()+" perguntas";

        }else{
            media = 10 * (acertos/questoes);//calcular media com a quantidade de acertos
            notaFinal = Mathf.RoundToInt(media); // arredondar a media
            if(notaFinal > PlayerPrefs.GetInt("Scene_Nota_Final"+ idTema.ToString())){
                PlayerPrefs.SetInt("notaFinal"+ idTema.ToString(), notaFinal);
                PlayerPrefs.SetInt("acertos" + idTema.ToString(), (int)acertos);
            }

            Application.LoadLevel("Scene_Nota_Final");
        }
        PlayerPrefs.SetInt("notaFinalTemp"+ idTema.ToString(), notaFinal);
        PlayerPrefs.SetInt("acertosTemp" + idTema.ToString(), (int)acertos);

            
    }
   

}
